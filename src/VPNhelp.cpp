#include "VPNhelp.h"

VPNhelp::VPNhelp()
{
}

VPNhelp::~VPNhelp()
{
}
// connect PPTP using ip, username, password
void VPNhelp::ConnectPPTP(PPTPSettings settings)
{	
	std::string pptpconfig = "pptp " + settings.ip + "file /etc/ppp/options.pptp user " + settings.user + " password " + settings.pass;
	FILE * fconnectPPTP = popen(pptpconfig.c_str(), "r");
	if (fconnectPPTP == 0) {
		fprintf(stderr, "Could not execute\n");
	}
	pclose(fconnectPPTP);
	std::fflush(stdin);
}
// 1 - connect openvpn  
// you can config --auth - user - pass on file *.opvn
void VPNhelp::ConnectOpenVPN(OpenvpnSettings settings)
{
	std::string openvpnconfig = "openvpn --config " + settings.pathFile + " --daemon";
	FILE * fconnectOpenVPN = popen(openvpnconfig.c_str(), "r");
	if (fconnectOpenVPN == 0) {
		fprintf(stderr, "Could not execute\n");
	}
	pclose(fconnectOpenVPN);
	std::fflush(stdin);
}

void VPNhelp::Disconnect()
{
	FILE * fDisconnect = popen("killall pppd openvpn", "r");
	if (fDisconnect == 0) {
		fprintf(stderr, "Could not execute\n");
	}
	pclose(fDisconnect);
	std::fflush(stdin);

}
