#pragma once
#include "VPNSettings.h"

class VPNhelp
{
	public:
		VPNhelp();
		virtual~VPNhelp();
		void ConnectPPTP(PPTPSettings settings);
		void ConnectOpenVPN(OpenvpnSettings settings);
		void Disconnect();
};

